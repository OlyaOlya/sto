﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data.OleDb;
namespace STO
{
    public class Db
    {
        public static SqlConnection cn;

        public static DataTable Query(string SQLstring)
        {
           
            var conn = new OleDbConnection();
            conn.ConnectionString =
                    "Provider=Microsoft.Jet.OLEDB.4.0; " +
                                      "Data Source=bd_STO.mdb";
           try
            {
            
                conn.Open();
                Console.WriteLine("Соедение успешно произведено");
            }
            catch (SqlException se)
            {
                MessageBox.Show("Ошибка подключения:{0}", se.Message);

            }

            using (OleDbCommand cm = new OleDbCommand(SQLstring, conn))
            {
                try
                {
                    OleDbDataAdapter da = new OleDbDataAdapter();

                da.SelectCommand = cm;
                    DataTable _table = new DataTable();
                    da.Fill(_table);
                    da.Dispose();
                    cm.Dispose();
                    conn.Close();
                    return _table;
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message);
                    cm.Dispose();
                    return null;
                }
            }
        }
    }
}
