﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace STO
{
    public partial class AddMaster : Form
    {
        public AddMaster()
        {
            InitializeComponent();
        }
         public AddMaster(string id, string name, string dat, string tel, 
                     string adr, string spec, string datP, string datU, string prich)
        {  
            InitializeComponent();

            button3.Visible = true;
            textBox2.Text = id;
            textBox1.Text = name;
            dateTimePicker1.Text = dat; 
            textBox3.Text = tel;
            textBox4.Text = adr;
            textBox5.Text = spec;
            dateTimePicker2.Text= datP;
            textBox6.Text = datU;
            textBox8.Text = prich;
            
         }

        private void button1_Click(object sender, EventArgs e)
        {
            // добавление мастера
            Db.Query("INSERT INTO Мастера (ФИО, дата_рождения, телефон, Адрес, Специализация, дата_приема, дата_увольнения, причина_увольнения ) " +
              " VALUES ('" + textBox1.Text + "', '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "', '"
              + textBox3.Text + "', '" + textBox4.Text + "', '"
              + textBox5.Text + "', '" + dateTimePicker2.Value.ToString("dd.MM.yyyy") + "',' "
              + textBox6.Text + "' , '" + textBox8.Text + "' )");
                   
            MessageBox.Show("Мастер добавлен");
            Close();
      
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //  try
            //{
            // редактирование инфо о мастере
            Db.Query("UPDATE Мастера SET  " +
                      " ФИО = '" + textBox1.Text + "', " +
                      " дата_рождения = '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "', " +
                      " телефон = '" + textBox3.Text + "', " +
                      " Адрес = '" + textBox4.Text + "', " +
                      " Специализация = '" + textBox5.Text + "', " +
                      " дата_приема = '" + dateTimePicker2.Value.ToString("dd.MM.yyyy") + "', " +
                      " дата_увольнения = '" + textBox6.Text + "', " +
                      " причина_увольнения = '" + textBox8.Text + "' " +
                      " WHERE id = " + textBox2.Text);

                MessageBox.Show("Изменения внесены");
                Close();
          /*  }
            catch
            {
                MessageBox.Show("Ошибка ввода");
            }*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AddMaster_Load(object sender, EventArgs e)
        {

        }
    }
}
