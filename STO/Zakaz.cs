﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace STO
{
    public partial class Zakaz : Form
    {
        public Zakaz()
        {
            InitializeComponent();
        }

        string id = "";

        string zakazchik = "";
        string sotrunik = "";

           public Zakaz(string id)
        {
                       
            InitializeComponent();

            this.id = id;

            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
            dateTimePicker1.Enabled = true;
            textBox2.Enabled = true;
            textBox4.Enabled = true;
            textBox6.Enabled = true;
            button4.Visible = false;

            dataGridView8.DataSource = Db.Query("SELECT * FROM Заказы WHERE id = " + id);

            if (dataGridView8.RowCount - 1 > 0)
            {
                textBox1.Text = id;
                zakazchik = dataGridView8[1, 0].Value.ToString();
                dateTimePicker1.Text = dataGridView8[2, 0].Value.ToString();
                textBox4.Text = dataGridView8[3, 0].Value.ToString();
                sotrunik = dataGridView8[4, 0].Value.ToString();

                dataGridView7.DataSource = Db.Query("SELECT Заказы.id, Услуги.название, Мастера.ФИО, Услуги.стоимость, Услуги.id " +
                    " FROM Услуги INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
                    "Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
                "ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
                    "WHERE  Заказы.id= " + textBox1.Text);
               
            }
      
        }
        double a = 0;
        
        int selectrow = -1;
        int selectrowUZ = -1;
       
        private void Zakaz_Load(object sender, EventArgs e)
        {

            textBox2.Text = a.ToString();
          
            dataGridView1.DataSource = Db.Query("SELECT id, ФИО FROM Мастера");
          
            if (dataGridView1.RowCount - 1 > 0)
            {
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    comboBox2.Items.Add(//dataGridView1[0, i].Value.ToString() + " " +
                                        dataGridView1[1, i].Value.ToString());
            }

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                if (sotrunik.Equals(dataGridView1[0, i].Value.ToString()))
                    comboBox2.SelectedIndex = i;
            }

            //выбор заказчика
          
            dataGridView2.DataSource = Db.Query("SELECT id, ФИО FROM Заказчики");
           
            if (dataGridView2.RowCount - 1 > 0)
            {
                for (int i = 0; i < dataGridView2.RowCount - 1; i++)
                    comboBox1.Items.Add(//dataGridView2[0, i].Value.ToString() + " " +
                                        dataGridView2[1, i].Value.ToString());
            }

            for (int i = 0; i < dataGridView2.RowCount - 1; i++)
            {
                if (zakazchik.Equals(dataGridView2[0, i].Value.ToString()))
                    comboBox1.SelectedIndex = i;
            }

            // все услуги

            dataGridView4.DataSource = Db.Query("SELECT id, название FROM Услуги");
            
                        // мастер услуги
            
            dataGridView6.DataSource = Db.Query(" SELECT id, ФИО FROM Мастера");
            if (dataGridView6.RowCount - 1 > 0)
            {
                for (int i = 0; i < dataGridView6.RowCount - 1; i++)
                    comboBox3.Items.Add(dataGridView6[0, i].Value.ToString() + " " +
                                        dataGridView6[1, i].Value.ToString());
            }

        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //отметить выделенную строку в таблице с заказами и отображение перечня услуг в заказе
            selectrow = dataGridView4.CurrentCell.RowIndex;

            dataGridView5.DataSource = null;

            if (selectrow < (dataGridView4.RowCount - 1) && selectrow != -1)
            {
                //вывод услуг из выбранного заказа

                if (dataGridView4[0, selectrow].Value.ToString() == "")
                    return;
                dataGridView5.DataSource = Db.Query(" SELECT * FROM Услуги " +
                                       " WHERE Услуги.id  = " + dataGridView4[0, selectrow].Value.ToString());

            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
          
            try
            {
                 Db.Query(" INSERT INTO Услуги_заказа ( id_заказа, id_услуги, id_мастера )  " +
                          " VALUES (" +
                          "  " + textBox1.Text + "  ," +
                          "  " + dataGridView5[0, 0].Value.ToString() + "  ," +
                          "  " + textBox3.Text);
                               
         //услуги в заказе
                dataGridView7.DataSource = Db.Query("SELECT Заказы.id, Услуги.название, Мастера.ФИО, Услуги.стоимость, Услуги.id " +
                " FROM Услуги INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
                "Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
                "ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
                "WHERE  Заказы.id= " + textBox1.Text);
             }
            catch
            {
                MessageBox.Show("Ошибка ввода данных");
            }
                
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
             if (comboBox3.SelectedIndex != -1)
            {
                textBox3.Text = dataGridView6[0, comboBox3.SelectedIndex].Value.ToString();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                Db.Query(" DELETE * FROM Услуги_заказа WHERE id_услуги = " +
                    dataGridView7[4, selectrowUZ].Value.ToString() +
                    "AND id_заказа = " + textBox1.Text);


                dataGridView7.DataSource = Db.Query("SELECT Заказы.id, Услуги.название, Мастера.ФИО, Услуги.стоимость, Услуги.id " +
                    " FROM Услуги INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
                    "Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
                "ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
                    "WHERE  Заказы.id= " + textBox1.Text);
                
                textBox6.Text = (Convert.ToInt32(textBox6.Text) - Convert.ToInt32(dataGridView7[0, 3].Value.ToString())).ToString();
                          
            }

            catch
            {
                MessageBox.Show("Ошибка выбора");
            }

        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrow = dataGridView4.CurrentCell.RowIndex;

            dataGridView5.DataSource = null;

            if (selectrow < (dataGridView4.RowCount - 1) && selectrow != -1)
            {
                //вывод услуг из выбранного заказа

                if (dataGridView4[0, selectrow].Value.ToString() == "")
                    return;

                dataGridView5.DataSource = Db.Query(" SELECT * " +
                                          " FROM Услуги " +
                                          " WHERE Услуги.id  = " + dataGridView4[0, selectrow].Value.ToString());
              
            }
        }

        private void dataGridView7_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowUZ = dataGridView7.CurrentCell.RowIndex;
        }

        private void dataGridView7_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowUZ = dataGridView7.CurrentCell.RowIndex;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            dataGridView7.DataSource = Db.Query("SELECT Заказы.id, Услуги.название, Мастера.ФИО, Услуги.стоимость, Услуги.id " +
                " FROM Услуги INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
                "Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
                "ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
                "WHERE  Заказы.id= " + textBox1.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Db.Query(" UPDATE Заказы SET " +
                  " id_заказчика = " + dataGridView2[0, comboBox1.SelectedIndex].Value.ToString() + ", " +
                  " дата_заказа = '" + dateTimePicker1.Value.ToString("dd.MM.yyyy") + "',  " +
                  " стоимость = " + textBox4.Text + " , " +
                  " id_мастера = " + dataGridView1[0, comboBox2.SelectedIndex].Value.ToString() + "  " +
                  " WHERE id = " + textBox1.Text);

                MessageBox.Show("Данные добавлены");
               // Close();
            }
            catch
            {
                MessageBox.Show("Ошибка ввода данных");
            }

        }
  
        private void button4_Click(object sender, EventArgs e)
        {
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
            dateTimePicker1.Enabled = true;
            textBox2.Enabled = true;
            textBox4.Enabled = true;
            textBox6.Enabled = true;

            dataGridView7.DataSource = null;
            //Формирование нового заказа
           
            Db.Query("INSERT INTO Заказы ( Стоимость )  " +
                  " VALUES ( 0 )");

            // id нового заказа
            
            dataGridView3.DataSource = Db.Query("SELECT  TOP 1   * FROM Заказы ORDER BY id DESC");
            textBox1.Text = dataGridView3[0, 0].Value.ToString();
           
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            double result = 0;
            if (dataGridView7.RowCount > 0)
            {
                for (int i = 0; i < dataGridView7.RowCount - 1; i++)
                {
                    
                        result += Convert.ToDouble(dataGridView7[3, i].Value.ToString()) ;
                        textBox6.Text = result.ToString();
                }

            }
            result += Convert.ToDouble(textBox2.Text);
            textBox4.Text = result.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Chek ch = new Chek(textBox1.Text.ToString());
            ch.Show();
        }

        }

 
        }

