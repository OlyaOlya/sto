﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace STO
{
    public partial class AddService : Form
    {
        public AddService()
        {
            InitializeComponent();
        }
        string vid = "";
        string id = "";
        public AddService(string id, string name, string type, string pr, string sum)
        {  
            InitializeComponent();

            this.id = id;
            button3.Visible = true;
         
            textBox3.Text = name;
            textBox5.Text = pr;
            textBox6.Text = sum;
           
            dataGridView2.DataSource = Db.Query("SELECT * FROM  ВидУслуги WHERE Название = '" + type + "'");
            if (dataGridView2.RowCount - 1 > 0)
            {
                vid = dataGridView2[1, 0].Value.ToString();
                textBox1.Text = id;
                
            }
         }

         
        private void AddService_Load(object sender, EventArgs e)
        {
            //заполнение combobox видом услуг
            dataGridView1.DataSource = Db.Query("Select * FROM  ВидУслуги");
            
            if (dataGridView1.RowCount - 1 > 0)
            {
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    comboBox1.Items.Add(//dataGridView1[0, i].Value.ToString() + " " +
                                        dataGridView1[1, i].Value.ToString());
            }
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                if (vid.Equals(dataGridView1[1, i].Value.ToString()))
                    comboBox1.SelectedIndex = i;
            }
         

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //добавление услуг
                Db.Query("INSERT INTO Услуги (название, id_вида, примечания, стоимость) " +

                    " VALUES ('" + textBox3.Text + "', " + dataGridView1[0, comboBox1.SelectedIndex].Value.ToString() +
                    ", '" + textBox5.Text + "'," + textBox6.Text + " )");

                MessageBox.Show("Услуга добавлена");
                Close();
            }
            catch
            {
                MessageBox.Show("Ошибка ввода данных");
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                //редактирование услуги
                Db.Query(" UPDATE Услуги SET  " +
                      " название = '" + textBox3.Text + "', " +
                      " id_вида = " + dataGridView1[0, comboBox1.SelectedIndex].Value.ToString() + ", " +
                      " примечания = '" + textBox5.Text + "', " +
                      " стоимость = " + textBox6.Text + " " +
                      " WHERE id = " + textBox1.Text);

               MessageBox.Show("Изменения внесены");
                Close();
            }
            catch
            {
                MessageBox.Show("Ошибка ввода данных");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
