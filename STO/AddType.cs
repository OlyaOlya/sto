﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace STO
{
    public partial class AddType : Form
    {
        public AddType()
        {
            InitializeComponent();
        }
        public AddType(string id, string name)
        {  
            InitializeComponent();

            button3.Visible = true;
            textBox1.Text = id;
            textBox3.Text = name;
        }
    
        private void button1_Click(object sender, EventArgs e)
        {
            //добавление вида услуг
            Db.Query(" INSERT INTO ВидУслуги (название) " +
                     " VALUES ('" + textBox3.Text + "'  )");

            MessageBox.Show("Вид услуги добавлен");
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //редактирование вида услуг
            Db.Query(" UPDATE ВидУслуги SET  " +
                  " Название = '" + textBox3.Text + "' " +
                  " WHERE id = " + textBox1.Text);
      
            MessageBox.Show("Изменения внесены");
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AddType_Load(object sender, EventArgs e)
        {

        }
    }
}
