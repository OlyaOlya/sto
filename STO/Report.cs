﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
namespace STO
{
    public partial class Report : Form
    {
        public Report()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " +
                                     "Data Source=bd_STO.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            string sql = "SELECT Заказы.id, Заказы.дата_заказа, Заказы.Стоимость, "+
                " Мастера.ФИО AS Мастера_ФИО, Заказчики.ФИО AS Заказчики_ФИО "+
                "FROM Мастера INNER JOIN ((Заказчики INNER JOIN Заказы ON " +
                "Заказчики.[id] = Заказы.[id_заказчика]) INNER JOIN Услуги_заказа "+
                " ON Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]";

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            ds.WriteXmlSchema("schema.xml");
            CrystalReport1 rpt = new CrystalReport1();
            rpt.SetDataSource(ds);
            crystalReportViewer1.ReportSource = rpt;
            connection.Close();
        }
    }
}
