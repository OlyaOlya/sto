﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace STO
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int selectrowU = -1;
        int selectrowT = -1;
        int selectrowZ = -1;
        int selectrowM = -1;
        int selectrowA = -1;

        int selectrowZak = -1;
      

        private void Form1_Load(object sender, EventArgs e)
        {
          
            dataGridView1.DataSource = Db.Query(" SELECT * " +
              " FROM ВидУслуги ");

            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[1].Width = 220;
           

            selectrowU = -1;
            if (dataGridView1.RowCount > 1)
            {
                selectrowU = 0;
               
                dataGridView7.DataSource = Db.Query("SELECT ВидУслуги.Название AS ВидУслуги, " +
                    " Услуги.id, Услуги.название AS название, Услуги.примечания, " +
                    "Услуги.стоимость FROM ВидУслуги INNER JOIN Услуги ON " +
                "ВидУслуги.[id] = Услуги.[id_вида] " +
                " WHERE Услуги.id_вида =" + dataGridView1[0, selectrowU].Value.ToString());

                dataGridView7.Columns[0].Width = 150;
                dataGridView7.Columns[1].Width = 25;
                dataGridView7.Columns[2].Width = 150;
                dataGridView7.Columns[3].Width = 150;
                dataGridView7.Columns[4].Width = 75;
             
                // мастера
                              
                dataGridView3.DataSource = Db.Query("SELECT * FROM Мастера ");
                selectrowM = -1;
                if (dataGridView3.RowCount > 0) selectrowM = 0;
                dataGridView3.Columns[0].Width = 25;

                //заказчики
               
                dataGridView8.DataSource = Db.Query(" SELECT * FROM Заказчики");
                dataGridView8.Columns[0].Width = 60;
                dataGridView8.Columns[1].Width = 150;
                dataGridView8.Columns[2].Width = 150;
                dataGridView8.Columns[3].Width = 300;
               
                // заказы

                dataGridView2.DataSource = Db.Query(" SELECT Заказы.id, Заказы.дата_заказа, Заказы.Стоимость, Заказы.id_мастера, Мастера.ФИО, Заказчики.ФИО  " +
                " FROM Заказы, Мастера, Заказчики " +
                " WHERE Заказы.id_мастера = Мастера.id  AND Заказы.id_заказчика = Заказчики.id");
               
                dataGridView2.Columns[0].Width = 35;
                dataGridView2.Columns[2].Width = 100;
                dataGridView2.Columns[3].Width = 0;
                dataGridView2.Columns[3].Visible = false;
                dataGridView2.Columns[4].Width = 150;
                dataGridView2.Columns[5].Width = 150;
               
                selectrowZak = -1;
                if (dataGridView2.RowCount > 1)
                {
                    selectrowZak = 0;
                    //услуги заказа
                    dataGridView6.DataSource = Db.Query("SELECT Заказы.id, Услуги.название AS Услуги_название, ВидУслуги.Название AS ВидУслуги_Название, " +
                        "Услуги.примечания, Услуги.стоимость, Мастера.ФИО " +
                        " FROM (ВидУслуги INNER JOIN Услуги ON ВидУслуги.[id] = Услуги.[id_вида]) " +
                        " INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
                        " Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
                        " ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
                        " WHERE Услуги_заказа.id_заказа = " + dataGridView2[0, selectrowZak].Value.ToString());
                                       
                    dataGridView6.Columns[0].Width = 35;

                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            selectrowU = dataGridView1.CurrentCell.RowIndex;
            selectrowT = dataGridView1.CurrentCell.RowIndex;
            dataGridView7.DataSource = null;

            if (dataGridView1[0, selectrowU].Value.ToString() == "")
                return;

            dataGridView7.DataSource = Db.Query("SELECT ВидУслуги.Название AS ВидУслуги, " +
                " Услуги.id, Услуги.название AS название, Услуги.примечания, " +
                "Услуги.стоимость FROM ВидУслуги INNER JOIN Услуги ON " +
            "ВидУслуги.[id] = Услуги.[id_вида] " +
            " WHERE Услуги.id_вида =" + dataGridView1[0, selectrowU].Value.ToString());
          
            dataGridView7.Columns[0].Width = 150;
            dataGridView7.Columns[1].Width = 25;
            dataGridView7.Columns[2].Width = 150;
            dataGridView7.Columns[3].Width = 150;
            dataGridView7.Columns[4].Width = 75;


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowU = dataGridView1.CurrentCell.RowIndex;
            selectrowT = dataGridView1.CurrentCell.RowIndex;
            dataGridView7.DataSource = null;

            if (dataGridView1[0, selectrowU].Value.ToString() == "")
                return;

            dataGridView7.DataSource = Db.Query("SELECT ВидУслуги.Название AS ВидУслуги, " +
                " Услуги.id, Услуги.название AS название, Услуги.примечания, " +
                "Услуги.стоимость FROM ВидУслуги INNER JOIN Услуги ON " +
            "ВидУслуги.[id] = Услуги.[id_вида] " +
            " WHERE Услуги.id_вида =" + dataGridView1[0, selectrowU].Value.ToString());
                       
            dataGridView7.Columns[0].Width = 150;
            dataGridView7.Columns[1].Width = 25;
            dataGridView7.Columns[2].Width = 150;
            dataGridView7.Columns[3].Width = 150;
            dataGridView7.Columns[4].Width = 75;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            AddType addS = new AddType();
            addS.Show();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            AddService addS = new AddService();
            addS.Show();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (dataGridView7.CurrentCell.RowIndex == -1 || dataGridView7.CurrentCell.RowIndex == (dataGridView7.RowCount - 1))
            {
                MessageBox.Show("Необходимо выбрать услугу для редактирования!");
                return;
            }

            AddService serv = new AddService(
                                     dataGridView7[1, dataGridView7.CurrentCell.RowIndex].Value.ToString(),
                                     dataGridView7[2, dataGridView7.CurrentCell.RowIndex].Value.ToString(),
                                     dataGridView7[0, dataGridView7.CurrentCell.RowIndex].Value.ToString(),
                                     dataGridView7[3, dataGridView7.CurrentCell.RowIndex].Value.ToString(),
                                     dataGridView7[4, dataGridView7.CurrentCell.RowIndex].Value.ToString());

            DialogResult res = serv.ShowDialog();
            if (res == DialogResult.OK)
            {
                Form1_Load(null, null);
            }
        }

        private void dataGridView7_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowU = dataGridView7.CurrentCell.RowIndex;
            selectrowT = dataGridView1.CurrentCell.RowIndex;

        }

        private void dataGridView7_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowU = dataGridView7.CurrentCell.RowIndex;
            selectrowT = dataGridView1.CurrentCell.RowIndex;

        }

        private void button23_Click(object sender, EventArgs e)
        {
            try
            {
                if (selectrowU == -1 || selectrowU == (dataGridView7.RowCount - 1))
                {
                    MessageBox.Show("Вы не выбрали услугу для удаления!");
                    return;
                }

                Db.Query("DELETE * FROM Услуги WHERE id = " + dataGridView7[1, selectrowU].Value.ToString()) ;
                MessageBox.Show("Услуга удалена");
                Form1_Load(null, null);
            }
            catch 
            {
                MessageBox.Show("Ошибка");
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex == -1)
            {
                MessageBox.Show("Выбирите критерий поиска");
                return;
            }

            string poisk = textBox2.Text;
            string sql = "SELECT ВидУслуги.Название AS ВидУслуги, " +
                    " Услуги.id, Услуги.название AS название, Услуги.примечания, " +
                    "Услуги.стоимость FROM ВидУслуги INNER JOIN Услуги ON " +
                "ВидУслуги.[id] = Услуги.[id_вида] " +
                " WHERE ";
            switch (comboBox2.SelectedItem.ToString())
            {

                case "название":
                    sql += " Услуги.название" + " like '" + poisk + "%' ";
                    break;
                case "примечания":
                    sql += "Услуги.примечания" + " like '" + poisk + "%'";
                    break;
                case "стоимость":
                    sql += " Услуги.стоимость= " + poisk + " ";
                    break;

            }
            dataGridView7.DataSource = Db.Query(sql);
        
            tabControl1.SelectedIndex = 0;

        }

        private void button25_Click(object sender, EventArgs e)
        {
            selectrowU = -1;
            if (dataGridView1.RowCount > 1)
            {
                selectrowU = 0;

                dataGridView7.DataSource = Db.Query("SELECT ВидУслуги.Название AS ВидУслуги, " +
                    " Услуги.id, Услуги.название AS название, Услуги.примечания, " +
                    "Услуги.стоимость FROM ВидУслуги INNER JOIN Услуги ON " +
                "ВидУслуги.[id] = Услуги.[id_вида] " +
                " WHERE Услуги.id_вида =" + dataGridView1[0, selectrowU].Value.ToString());

                dataGridView7.Columns[0].Width = 150;
                dataGridView7.Columns[1].Width = 25;
                dataGridView7.Columns[2].Width = 150;
                dataGridView7.Columns[3].Width = 150;
                dataGridView7.Columns[4].Width = 75;
                comboBox2.SelectedIndex = -1;
                textBox2.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (selectrowT == -1 || selectrowT == (dataGridView1.RowCount - 1))
            {
                MessageBox.Show("Необходимо выбрать услугу для редактирования!");
                return;
            }

            AddType type = new AddType(
                                     dataGridView1[0, selectrowT].Value.ToString(),
                                     dataGridView1[1, selectrowT].Value.ToString());

            DialogResult res = type.ShowDialog();
            if (res == DialogResult.OK)
            {
                Form1_Load(null, null);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (selectrowT == -1 || selectrowT == (dataGridView1.RowCount - 1))
            {
                MessageBox.Show("Вы не выбрали услугу для удаления!");
                return;
            }

            MessageBox.Show("Будут удалены вид услуги и все услуги этого вида");
            Db.Query("DELETE * FROM Услуги WHERE id_вида = " + dataGridView1[0, selectrowT].Value.ToString());
            Db.Query("DELETE * FROM ВидУслуги WHERE id = " + dataGridView1[0, selectrowT].Value.ToString());
            MessageBox.Show("Вид услуги удален");
            Form1_Load(null, null);
        }

        private void button27_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Db.Query(" SELECT * " +
                  " FROM ВидУслуги ");
                   
            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[1].Width = 220;
          

            selectrowU = -1;
            if (dataGridView1.RowCount > 1)
            {
                selectrowU = 0;
                dataGridView7.DataSource = Db.Query("SELECT ВидУслуги.Название AS ВидУслуги, " +
                     " Услуги.id, Услуги.название AS название, Услуги.примечания, " +
                     "Услуги.стоимость FROM ВидУслуги INNER JOIN Услуги ON " +
                 "ВидУслуги.[id] = Услуги.[id_вида] " +
                 " WHERE Услуги.id_вида =" + dataGridView1[0, selectrowU].Value.ToString());
                              
                dataGridView7.Columns[0].Width = 150;
                dataGridView7.Columns[1].Width = 25;
                dataGridView7.Columns[2].Width = 150;
                dataGridView7.Columns[3].Width = 150;
                dataGridView7.Columns[4].Width = 75;
               
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Выбирите критерий поиска");
                return;
            }

            string poisk = textBox1.Text;
            dataGridView1.DataSource = Db.Query("SELECT * FROM ВидУслуги" +
               " WHERE  Название" + " like '" + poisk + "%' ");
                      
            tabControl1.SelectedIndex = 0;
        }

        private void button24_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
            textBox1.Text = "";
            dataGridView1.DataSource = Db.Query(" SELECT * " +
             " FROM ВидУслуги ");

            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[1].Width = 220;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            AddMaster addM = new AddMaster();
            addM.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (selectrowM == -1 || selectrowM == (dataGridView3.RowCount - 1))
            {
                MessageBox.Show("Необходимо выбрать мастера для редактирования!");
                return;
            }


            AddMaster master = new AddMaster(
                                     dataGridView3[0, selectrowM].Value.ToString(),
                                     dataGridView3[1, selectrowM].Value.ToString(),
                                     dataGridView3[2, selectrowM].Value.ToString(),
                                     dataGridView3[3, selectrowM].Value.ToString(),
                                     dataGridView3[4, selectrowM].Value.ToString(),
                                     dataGridView3[5, selectrowM].Value.ToString(),
                                     dataGridView3[6, selectrowM].Value.ToString(),
                                     dataGridView3[7, selectrowM].Value.ToString(),
                                     dataGridView3[8, selectrowM].Value.ToString());

            DialogResult res = master.ShowDialog();
            if (res == DialogResult.OK)
            {
                Form1_Load(null, null);
            }
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowM = dataGridView3.CurrentCell.RowIndex;
        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowM = dataGridView3.CurrentCell.RowIndex;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (selectrowM == -1 || selectrowM == (dataGridView3.RowCount - 1))
            {
                MessageBox.Show("Вы не выбрали мастера для удаления!");
                return;
            }
            Db.Query("DELETE * FROM Мастера WHERE id = " + dataGridView3[0, selectrowM].Value.ToString()) ;
           
            MessageBox.Show("Информация о мастере удалена");
            Form1_Load(null, null);
        }

        private void button30_Click(object sender, EventArgs e)
        {
            dataGridView3.DataSource = Db.Query(" SELECT * FROM Мастера");
        }

        private void button29_Click(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex == -1)
            {
                MessageBox.Show("Выбирите критерий поиска");
                return;
            }
            
            string poisk = textBox3.Text;
            string sql = "SELECT * FROM Мастера " +
                " WHERE ";
            switch (comboBox3.SelectedItem.ToString())
            {

                case "ФИО":
                    sql += " ФИО" + " like '" + poisk + "%' ";
                    break;
                case "Телефон":
                    sql += " телефон " + " like '" + poisk + "%'";
                    break;
                case "Специализация":
                    sql += " Специализация  " + " like '" + poisk + "%'";
                    break;
                case "Дата приема":
                    sql += " дата_приема  " + " like '" + poisk + "%'";
                    break;
                case "Дата увольнения":
                    sql += " дата_увольнения  " + " like '" + poisk + "%'";
                    break;

            }

            dataGridView3.DataSource = Db.Query(sql);
            tabControl1.SelectedIndex = 2;
        }

        private void dataGridView8_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowA = dataGridView8.CurrentCell.RowIndex;
            selectrowZ = dataGridView8.CurrentCell.RowIndex;
        }

        private void dataGridView8_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowA = dataGridView8.CurrentCell.RowIndex;
            selectrowZ = dataGridView8.CurrentCell.RowIndex;
        }

        private void button34_Click(object sender, EventArgs e)
        {
            AddZakazchik Addz = new AddZakazchik();
            Addz.Show();
        }


        private void button9_Click(object sender, EventArgs e)
        {
            dataGridView8.DataSource = Db.Query(" SELECT * FROM Заказчики");

            dataGridView8.Columns[0].Width = 60;
            dataGridView8.Columns[1].Width = 150;
            dataGridView8.Columns[2].Width = 150;
            dataGridView8.Columns[3].Width = 300;
        }

        private void button35_Click(object sender, EventArgs e)
        {
            if (selectrowZ == -1 || selectrowZ == (dataGridView3.RowCount - 1))
            {
                MessageBox.Show("Необходимо выбрать заказчика для редактирования!");
                return;
            }

            AddZakazchik zakazchik = new AddZakazchik(
                                      dataGridView8[0, selectrowZ].Value.ToString(),
                                      dataGridView8[1, selectrowZ].Value.ToString(),
                                      dataGridView8[2, selectrowZ].Value.ToString(),
                                      dataGridView8[3, selectrowZ].Value.ToString());

            DialogResult res = zakazchik.ShowDialog();
            if (res == DialogResult.OK)
            {
                Form1_Load(null, null);
            }
        }

        private void button36_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (selectrowZ == -1 || selectrowZ == (dataGridView3.RowCount - 1))
                {
                    MessageBox.Show("Вы не выбрали заказчика для удаления!");
                    return;
                }

                Db.Query("DELETE * FROM Заказы WHERE id_заказчика = " + dataGridView8[0, selectrowZ].Value.ToString());
                Db.Query("DELETE * FROM Заказчики WHERE id = " + dataGridView8[0, selectrowZ].Value.ToString()) ;
             
                MessageBox.Show("Информация о заказчике удалена");
                Form1_Load(null, null);
            }
            catch
            {
                MessageBox.Show("Ошибка");
            }

        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (comboBox5.SelectedIndex == -1)
            {
                MessageBox.Show("Выбирите критерий поиска");
                return;
            }


            string poisk = textBox5.Text;
            string sql = "SELECT * FROM Заказчики " +
                " WHERE ";
            switch (comboBox5.SelectedItem.ToString())
            {

                case "ФИО":
                    sql += " ФИО" + " like '" + poisk + "%' ";
                    break;
                case "Телефон":
                    sql += " Телефон " + " like '" + poisk + "%'";
                    break;
                case "Автомобиль":
                    sql += " Полное_описание_автомобиля  " + " like '" + poisk + "%'";
                    break;


            }

            dataGridView8.DataSource = Db.Query(sql);
            tabControl1.SelectedIndex = 3;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            comboBox3.SelectedIndex = -1;
            textBox3.Text = "";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            comboBox5.SelectedIndex = -1;
            textBox5.Text = "";
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //отметить выделенную строку в таблице с заказом и отображение перечня услуг в заказе
            selectrowZak = dataGridView2.CurrentCell.RowIndex;
            dataGridView6.DataSource = null;

            if (selectrowZak < (dataGridView2.RowCount - 1) && selectrowZak != -1)
            {
                if (dataGridView2[0, selectrowZak].Value.ToString() == "")
                    return;

                dataGridView6.DataSource = Db.Query("SELECT Заказы.id, Услуги.название AS Услуги_название, ВидУслуги.Название AS ВидУслуги_Название, " +
               "Услуги.примечания, Услуги.стоимость, Мастера.ФИО " +
               " FROM (ВидУслуги INNER JOIN Услуги ON ВидУслуги.[id] = Услуги.[id_вида]) " +
               " INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
               " Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
               " ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
               " WHERE Услуги_заказа.id_заказа = " + dataGridView2[0, selectrowZak].Value.ToString());
                dataGridView6.Columns[0].Width = 35;
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectrowZak = dataGridView2.CurrentCell.RowIndex;
            dataGridView6.DataSource = null;

            if (selectrowZak < (dataGridView2.RowCount - 1) && selectrowZak != -1)
            {

                if (dataGridView2[0, selectrowZak].Value.ToString() == "")
                    return;

                dataGridView6.DataSource = Db.Query("SELECT Заказы.id, Услуги.название AS Услуги_название, ВидУслуги.Название AS ВидУслуги_Название, " +
               "Услуги.примечания, Услуги.стоимость, Мастера.ФИО " +
               " FROM (ВидУслуги INNER JOIN Услуги ON ВидУслуги.[id] = Услуги.[id_вида]) " +
               " INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
               " Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
               " ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
               " WHERE Услуги_заказа.id_заказа = " + dataGridView2[0, selectrowZak].Value.ToString());
                dataGridView6.Columns[0].Width = 35;
          
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (selectrowZak == -1 || selectrowZak == (dataGridView2.RowCount - 1))
                {
                    MessageBox.Show("Вы не выбрали заказ для удаления!");
                    return;
                }


                Db.Query("DELETE * FROM Услуги_заказа WHERE id_заказа = " + dataGridView2[0, selectrowZak].Value.ToString()) ;
                string kod = dataGridView2[0, selectrowZak].Value.ToString();
                Db.Query("DELETE * FROM Заказы WHERE id = " + kod);

                Form1_Load(null, null);
            }
            catch
            {
                MessageBox.Show("Ошибка");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Zakaz zakaz = new Zakaz();
            zakaz.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            button3.Visible = true;
        /*    if (selectrowZak == -1 || selectrowZak == (dataGridView3.RowCount - 1))
            {
                MessageBox.Show("Вы не выбрали заказ для редактирования!");
                return;
            }*/

            //иначе вызов формы редактирования заказа
            Zakaz zak = new Zakaz(dataGridView2[0, selectrowZak].Value.ToString());
            DialogResult result = zak.ShowDialog();
            //обновление всех таблиц на форме после редактирования заказа
            Form1_Load(null, null);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = Db.Query(" SELECT Заказы.id, Заказы.дата_заказа, Заказы.Стоимость, Заказы.id_мастера, Мастера.ФИО, Заказчики.ФИО  " +
                     " FROM Заказы, Мастера, Заказчики " +
                     " WHERE Заказы.id_мастера = Мастера.id  AND Заказы.id_мастера = Заказчики.id");
                      
            dataGridView2.Columns[0].Width = 35;
            dataGridView2.Columns[2].Width = 100;
            dataGridView2.Columns[3].Width = 0;
            dataGridView2.Columns[3].Visible = false;
            dataGridView2.Columns[4].Width = 150;
            dataGridView2.Columns[5].Width = 150;
           
            selectrowZak = -1;
            if (dataGridView2.RowCount > 1)
            {
                selectrowZak = 0;

                //услуги заказа
                dataGridView6.DataSource = Db.Query("SELECT Заказы.id, Услуги.название AS Услуги_название, ВидУслуги.Название AS ВидУслуги_Название, " +
                    "Услуги.примечания, Услуги.стоимость, Мастера.ФИО " +
                    " FROM (ВидУслуги INNER JOIN Услуги ON ВидУслуги.[id] = Услуги.[id_вида]) " +
                    " INNER JOIN (Мастера INNER JOIN (Заказы INNER JOIN Услуги_заказа ON " +
                    " Заказы.[id] = Услуги_заказа.[id_заказа]) ON Мастера.[id] = Услуги_заказа.[id_мастера]) " +
                    " ON Услуги.[id] = Услуги_заказа.[id_услуги] " +
                    " WHERE Услуги_заказа.id_заказа = " + dataGridView2[0, selectrowZak].Value.ToString());

                dataGridView6.Columns[0].Width = 35;

            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (comboBox4.SelectedIndex == -1)
            {
                MessageBox.Show("Выбирите критерий поиска");
                return;
            }



            string poisk = textBox4.Text;
            string sql = " SELECT Заказы.id, Заказы.дата_заказа, Заказы.Стоимость, Заказы.id_мастера, Мастера.ФИО, Заказчики.ФИО  " +
                         " FROM Заказы, Мастера, Заказчики " +
                         " WHERE Заказы.id_мастера = Мастера.id  AND Заказы.id_мастера = Заказчики.id ";
            switch (comboBox4.SelectedItem.ToString())
            {

                case "Дата":
                    sql += "AND Заказы.дата_заказа" + " like '" + poisk + "%' ";
                    break;
                case "Стоимость":
                    sql += " AND Заказы.Стоимость " + " like '" + poisk + "%'";
                    break;
                case "Мастер":
                    sql += " AND Мастера.ФИО  " + " like '" + poisk + "%'";
                    break;
                case "Заказчик":
                    sql += " AND Заказчики.ФИО  " + " like '" + poisk + "%'";
                    break;
                

            }

            dataGridView2.DataSource = Db.Query(sql);
           
            tabControl1.SelectedIndex = 1;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            comboBox4.SelectedIndex = -1;
            textBox4.Text = "";
        
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Report rep = new Report();
            rep.Show();
        }
    }
}