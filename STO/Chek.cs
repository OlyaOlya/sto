﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace STO
{
    public partial class Chek : Form
    {
        public Chek()
        {
            InitializeComponent();
        }

        public Chek(string id)
        {
            InitializeComponent();

            textBox1.Text = id;
        }

        private void Chek_Load(object sender, EventArgs e)
        {
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " +
"Data Source=bd_STO.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;

            
            string sql = "SELECT Заказы.id, Заказы.дата_заказа, Мастера.ФИО, Услуги_заказа.id_услуги, " +
                " Услуги.название, Услуги.стоимость, Заказы.Стоимость " +
                " FROM Услуги INNER JOIN (Мастера INNER JOIN ((Заказчики INNER JOIN Заказы " + 
                " ON Заказчики.id = Заказы.id_заказчика) INNER JOIN Услуги_заказа ON " +
                " Заказы.id = Услуги_заказа.id_заказа) ON Мастера.id = Услуги_заказа.id_мастера) " +
                " ON Услуги.id = Услуги_заказа.id_услуги " +
                " WHERE Заказы.id = " + textBox1.Text;

                
                
                /*" SELECT Заказы.id, Заказы.дата_заказа, Заказы.Стоимость, Заказы.id_мастера, Мастера.ФИО, Заказчики.ФИО  " +
       " FROM Заказы, Мастера, Заказчики " +
       " WHERE Заказы.id_мастера = Мастера.id  AND Заказы.id_заказчика = Заказчики.id " +
       " WHERE Заказы.id = " + textBox1.Text;

            /*" SELECT Продажи.id, Сотрудники.ФИО, Медикаменты.Название, Перечень_покупок.Колличество, " +
            " Медикаменты.Цена, " +
            " Перечень_покупок.Колличество * Медикаменты.Цена as Сумма " +
            " FROM (Сотрудники INNER JOIN Продажи ON Сотрудники.[id] = Продажи.[id_сотрудника]) " +
            " INNER JOIN (Медикаменты INNER JOIN Перечень_покупок ON Медикаменты.[id] = Перечень_покупок.[id_товара]) " +
            " ON Продажи.[id] = Перечень_покупок.[id_продажи] " +
            "WHERE Продажи.id= " + textBox1.Text;*/

            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Chek1");
            ds.WriteXmlSchema("schema2.xml");

            CrystalReport2 rpt = new CrystalReport2();
            rpt.SetDataSource(ds);
            crystalReportViewer1.ReportSource = rpt;

            connection.Close();
        }
    }
}
